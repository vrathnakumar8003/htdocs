$(document).ready(function () {

    $('#loginBtn').on('click', function (e) {
        e.preventDefault();

        let username = document.getElementById("username");
        let password = document.getElementById("password");

        $.ajax({
            url: 'php/TreasureHunt.php',
            type: 'get',
            data: {'action': 'login', 'username': username.value, 'password': password.value},
            success: function (data, status) {

                if ($.trim(data) == "Success") {
                    window.location='pages/showchallenges.php';
                } else {
                    alert(data);
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            }
        });
    });

    $('#signupBtn').on('click', function (e) {

        e.preventDefault();

        let username = document.getElementById("signupUsername");
        let password = document.getElementById("signupPassword");
        let email = document.getElementById("email");
        let reEnterPassword = document.getElementById("reEnterPassword");

        $.ajax({
            url: 'php/TreasureHunt.php',
            type: 'post',
            data: {'action': 'sign_up', 'username': username.value, 'password': password.value,'email':email.value, 'reEnterPassword':reEnterPassword.value},
            success: function (data, status) {

                if ($.trim(data) == "Created") {
                    alert(data);
                } else {
                    alert(data);
                }
            },
            error: function (xhr, desc, err) {

                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            }
        });
    });
});