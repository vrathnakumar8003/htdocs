
 drop database WebDesign;

CREATE DATABASE WebDesign;

USE WebDesign;

SELECT DATABASE();

CREATE TABLE Game(
UserID INT UNSIGNED NOT NULL PRIMARY KEY auto_increment,
UserName VARCHAR(40) NOT NULL unique,
Password VARCHAR(30) NOT NULL,
email varchar(255) null,
Level int NOT NULL default 0,
Points int NOT NULL default 100
);

insert into Game(UserName,Password,email)
	values('Admin','123','admin@teamfive.com');
    

USE WebDesign;

CREATE TABLE Questions(
QuestionID INT UNSIGNED NOT NULL PRIMARY KEY auto_increment,
QuestionIndex int NOT NULL unique,
Question VARCHAR(255) NOT NULL,
Answer VARCHAR(255) NOT NULL,
QuestionHint VARCHAR(255) NOT NULL,
QuestionLevel int NOT NULL,
QuestionPoints int NOT NULL
);

insert into Questions(QuestionIndex,Question,Answer,QuestionHint,QuestionLevel,QuestionPoints)
	values(1,'What are the names of the staff members who work at the welcome desk?','Michelle and Joanne','Start With Mic',2,20),
    (2,'What is the name of the Waterloo Campus Resturant?','Bloom','Ends with om',1,10),
    (3,'Who is John Tibbits?','President of Conestoga College','Boss!',1,10),
    (4,'How much the lowest cost Conestoga T-Shirt Cost?','$14.95','Less than $15',1,10),
    (5,'What are the warnings on the daycare door related to?','Immunization records & nuts are not allowed','2 things',3,30),
    (6,'Where is the bakery?','Second floor in the A Wing.','In A wing',2,20),
    (7,'Which collaborative rooms in the library can hold 7 or more Students','Room 3 and Wing A','There are 2 rooms',2,20);