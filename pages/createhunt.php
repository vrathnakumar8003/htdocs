<?php
   ob_start();
   session_start();
   //connection to a database
	require_once('mysqli_connect.php');
	
?>
<?php
  $msg = '';
            
            if (isset($_POST['submit']) && !empty($_POST['email']) 
               && !empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['re-enter-password']) &&($_POST['password']==$_POST['re-enter-password']) ) {
	
		
			$query="insert into Game (UserName,Password,email) values('".
			$_POST['username']."','".
			$_POST['password']."','".
			$_POST['email']."')";
			
			$result= mysqli_query($link,$query);
			if($result){
				$msg='You Signed Up Successfully!!'.
				//"</br>".
				'<a href="../index.php">'."Click here"."</a>".
				' to go to login page';
			
               }
			   else{
				   $msg='Unable to sign up! Please contact the administrator...';
			   }
            }else {
                  $msg = 'Enter the fields correctly';
           }
           $msg = '<div class="alert alert-primary" role="alert">'. $msg .'</div>';

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../styles/css/bootstrap.css">
    <link rel="stylesheet" href="../styles/main.css">
    <title>Treasure Hunt</title>
</head>
<body>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="#">Treasure Hunt</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <!-- hide if not logged in -->
          <!-- <li class="nav-item active">
            <a class="nav-link" href="#">Points: <span class="sr-only">(current)</span></a>
          </li> -->
          <!-- hide if logged in -->
          <li class="nav-item">
          <a class="nav-link" href="pages/signup.php">Sign Up</a>
          </li>
            <!-- hide if not logged in -->
          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Challenge Name</a> -->
            <!-- <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Challenge 1</a>
              <a class="dropdown-item" href="#">Challenge 2</a>
              <a class="dropdown-item" href="#">Challenge 3</a>
            </div> -->
          <!-- </li> -->
        </ul>
        <form class="form-inline my-2 my-lg-0" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
            <!-- move login up to nav on every page but the landing -->
        <input class="form-control mr-sm-1" type="text" name="username" placeholder="Username">
        <input class="form-control mr-sm-1" type="password" name="password" placeholder="Password">
         <!-- change this if logged in so it logs you out -->
         <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Login</button>
        </form>
      </div>
      
    </nav>
    <main role="main" class="container" style="padding-top: 65px">
            <h1>Create/Edit Hunt</h1>
            <?php echo $msg;?>
            <h4>Challenge 1</h4>
            <form action="#" method="POST">
                <label for="hunt_name">Name</label>
                <input class="form-control mr-auto" type="text" name="hunt_name" id="hunt_name">
                <label for="hunt_question_1">Question</label>
                <input class="form-control mr-auto" type="text" name="hunt_question_1">
                <label for="hunt_answer_1">Answer</label>
                <input class="form-control mr-auto" type="text" name="hunt_answer_1">
                <label for="hunt_clue_1">Clue</label>
                <input class="form-control mr-auto" type="text" name="hunt_clue_1">
                <button class="btn btn-lg">Add Clue</button>
                <button class="btn btn-lg">Add Challenge</button>
                <br>
                <button class="btn btn-group-lg btn-success" type = "submit" name = "submit">Create / Update</button>

            </form>`
        </main>
</body>
<script src="..\scripts\jslib\jquery-1.10.2.min.js"></script>
<script src="../scripts/js/bootstrap.js"></script>
<!--script src="scripts/main.js"></script-->
</html>