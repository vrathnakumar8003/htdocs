<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../styles/css/bootstrap.css">
    <link rel="stylesheet" href="../styles/main.css">
    <title>Treasure Hunt</title>
</head>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="#">Treasure Hunt</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class ="nav-item active">
                <a class="nav-link" href="">Current Point Count</a>
            </li>
          <!-- hide if not logged in -->
          <!-- <li class="nav-item active">
            <a class="nav-link" href="#">Points: <span class="sr-only">(current)</span></a>
          </li> -->
          <!-- hide if logged in -->
          <!-- <li class="nav-item">
          <a class="nav-link" href="pages/signup.php">Sign Up</a>
          </li> -->
            <!-- hide if not logged in -->
           <li class="nav-item dropdown"> 
            <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">This Hunt</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Challenge 1</a>
              <a class="dropdown-item" href="#">Challenge 2</a>
              <a class="dropdown-item" href="#">Challenge 3</a>
            </div> 
           </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
            <!-- move login up to nav on every page but the landing -->
        <!-- <input class="form-control mr-sm-1" type="text" name="username" placeholder="Username"> -->
        <!-- <input class="form-control mr-sm-1" type="password" name="password" placeholder="Password"> -->
         <!-- change this if logged out so it logs you in -->
         <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Log Out</button>
        </form>
      </div>
</nav>
<body>
    <!-- add 65px to padding top -->
    <main role="main" class="container" style="padding-top: 65px">
       <!-- figure out breakpoints -->
        <h1>Show Challenges</h1>
            <div class="row">
                <a href="challenge.php?challenge=1">
                    <div class="col col-md-8 col-lg-12">
                        <div class="card">
                        <img class="card-img-top" src="..\Resources\unlock-solid.svg" alt="Challenge Locked">
                        <div class="card-body">
                            <h5 class="card-title">Challenge 1</h5>
                        </div>
                        </div>
                    </div>
                </a>
                <a href="challenge.php?challenge=2">
                    <div class="col col-md-8 col-lg-12">
                    <div class="card">
                        <img class="card-img-top" src="..\Resources\lock-solid.svg" alt="Challenge Locked">
                        <div class="card-body">
                            <h5 class="card-title">Challenge 2</h5>
                        </div>
                        </div>
                    </div>
                </a>
                <a href="challenge.php?challenge=3">
                    <div class="col  col-md-8 col-lg-12">
                    <div class="card">
                        <img class="card-img-top" src="..\Resources\lock-solid.svg" alt="Challenge Locked">
                        <div class="card-body">
                            <h5 class="card-title">Challenge 3</h5>
                        </div>
                        </div>
                    </div>
                </a>
            </div>

            </div>
        <!-- <h2>This is where the question goes</h2>
        <form action="#" method="POST">
            <input class="form-control mr-auto" type="text" name="answer">
            <br>
            <input class="btn btn-lg btn-success" type="submit">

        </form>
        <a class="btn btn-secondary" href="">Clue 100 Points</a>
        <br>
        <ul>
            <li><a href="#">This is a hint</a></li>
            <li><a href="#">This is another hint</a></li>
        </ul> -->
    </main>
</body>
<script src="..\scripts\jslib\jquery-1.10.2.min.js"></script>
<script src="../scripts/js/bootstrap.js"></script>
<!--script src="../scripts/main.js"></script-->
</html>